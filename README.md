# Synopsis

XPlane is a beautiful and powerful flight simulator. 
It can also be used for create, simulate and try your customs realtime systems like computer board for an , PID controller, etc. 

[XPlane official website](http://www.x-plane.com/)

# Prerequisites

 * XPlane 11 or greater
 * UDP receive port configured to 49000 (see screenshot)
 * UDP sending port configured to 49001 (see screenshot)
 
![XPlane configuration](screenshots/xplane-configuration.png) 

# How to run

1. First, you have to run XPlane. 

![XPlane splashscreen](screenshots/xplane-splashscreen.png) 

2. Next, run a new flight or resume last flight

![XPlane flight](screenshots/xplane-flight.png)

3. Finally, run your python program.



# Example

See an example [here](example.py)

# Motivation

The motivation was to try to create some flying models for automatic takeoff and landing.

# References

## PID Controller

[PID Controller wikipedia](https://en.wikipedia.org/wiki/PID_controller)
[Michigan University control tutorial](http://ctms.engin.umich.edu/CTMS/index.php?example=Introduction&section=ControlPID)

## Simulink & MATLab

Add some references

# Licence

Details of MIT licence [here](LICENSE.txt)
