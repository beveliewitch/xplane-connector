class PIDController:

    integral = 0
    derivative = 0
    previous_error = 0
    error_sum = 0
    dt = 0.001

    def __init__(self, kp, ki, kd, low_limit=0.0, up_limit=0.0):
        self.kp = kp
        self.ki = ki
        self.kd = kd
        self.low_limit = low_limit
        self.up_limit = up_limit

    def bounded(self, value, min, max):
        return min < value < max

    def output(self, set_point, process_variable):
        err = set_point - process_variable
        self.integral += err * self.dt

        p = self.kp * err
        i = self.ki * self.error_sum * self.dt
        d = self.kd * ((err - self.previous_error) / self.dt)
        # d = 0
        output = p + i + d

        self.previous_error = err

        if self.low_limit != self.up_limit and not self.bounded(output, self.low_limit, self.up_limit):
            self.error_sum = err
        else:
            self.error_sum += err

        if self.low_limit != self.up_limit:
            output = max(min(output, self.up_limit), self.low_limit)

        return output